const { expect } = require('@wdio/globals')
const LoginPage = require('../pageobjects/login.page')
const MainPage = require('../pageobjects/main.page')
const ReceivingPage = require('../pageobjects/receiving.page')

describe('Login', () => {
    it('should login with valid credentials', async () => {
        await LoginPage.open()
        await LoginPage.login('merabby', 'Test1234!')
        await expect(MainPage.title).toBeExisting()
    })
})

describe('Receiving', () => {
    it('should open receiving', async () => {
        await MainPage.open()
        await MainPage.receiving()
        await expect(ReceivingPage.title).toBeExisting()
        await MainPage.back()
    })

    it('should show error when scanning unknown receiving ID', async () => {
        await MainPage.open()
        await MainPage.receiving()
        await expect(ReceivingPage.title).toBeExisting()
        await MainPage.scan("something")
        await expect(ReceivingPage.scanError).toBeExisting()
        await MainPage.back()
    })
})