const { $ } = require('@wdio/globals')
const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class MainPage extends Page {
    /**
     * define selectors using getter methods
     */
    get title() {
        return $('xpath://android.widget.TextView[@text="Optiturn -- Staging"]');
    }

    get btnReceiving() {
        return $('~Receiving');
    }

    get btnRework() {
        return $('xpath://android.view.View[@text="Receiving"]');
    }

    get btnMenu() {
        return $('xpath://android.widget.ImageButton[@content-desc="Navigate up"]');
    }

    get btnLogout() {
        return $('xpath://android.widget.CheckedTextView[@resource-id="com.optoro.optiturn.qa:id/design_menu_item_text" and @text="Logout"]');
    }

    async logout() {
        await this.menu.click();

        await driver.touchAction([
            { action: 'press', x: 561, y: 2058 },
            { action: 'moveTo', x: 573, y: 850 },
            'release'
          ]);

        await this.btnLogout.click();
    }

    async receiving() {
        await this.btnReceiving.click();
    }

}

module.exports = new MainPage();
