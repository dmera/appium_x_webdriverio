const { browser } = require('@wdio/globals')

/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
module.exports = class Page {
    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
    open (path) {
        driver.activateApp("com.optoro.optiturn.qa")
    }

    async scan(barcode) {
        await driver.executeScript('mobile: shell', [{
            command: 'am broadcast -a com.optoro.optiturn.SCAN_CODE --es com.symbol.datawedge.data_string ' + '\"' + barcode + '\"',
            args: [],
        }]);
    }
    
    async back() {
        await driver.back()
    }
}