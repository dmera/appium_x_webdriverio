const { $ } = require('@wdio/globals')
const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
    get inputUsername () {
        return $("id:com.optoro.optiturn.qa:id/login_edit_text");
    }

    get inputPassword () {
        return $("id:com.optoro.optiturn.qa:id/password_edit_text");
    }

    get btnNext () {
        return $("id:com.optoro.optiturn.qa:id/next_button");
    }

    get btnLogin () {
        return $("id:com.optoro.optiturn.qa:id/login_button");
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async login (username, password) {
        await this.inputUsername.setValue(username);
        await this.btnNext.click();
        await this.inputPassword.setValue(password);
        await this.btnLogin.click();
    }

    // /**
    //  * overwrite specific options to adapt it to page object
    //  */
    // open () {
    //     //return super.open('login');
    // }
}

module.exports = new LoginPage();
