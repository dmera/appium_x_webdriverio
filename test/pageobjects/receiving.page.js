const { $ } = require('@wdio/globals')
const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ReceivingPage extends Page {
    /**
     * define selectors using getter methods
     */
    get title() {
        return $('xpath://android.widget.Image[@text="Receiving%20ID"]');
    }

    get scanError() {
        return $('xpath://android.widget.TextView[@text="Invalid Receiving ID. Please try again."]');
    }

}

module.exports = new ReceivingPage();