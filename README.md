# Apppium + WebdriverIO

This is a proof of concept to see how Appium can be used for doing End-to-End automation testings on the android Optiturn native app.

## Requiriments

First of all, we will need the Android SDK installed

https://developer.android.com/studio

Once you have Android Studio installed and running, then you will need to set a few environment variables. This is very important since Appium will heavily use the command `adb` and has to be reached from terminal.

You can archive this by adding this at the end of your `.bashrc` file. I am assuming you are using MacOS, I know, life is unfair. 
```bash
export ANDROID_HOME=/Users/$USER/Library/Android/sdk
export PATH="$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools"
```

For more info check:
https://github.com/appium/java-client/blob/master/docs/environment.md#how-to-fix-missing-environment-variables


Then we need to be sure we are using the right node version. At the moment this project was coded, Appium integration on WebdriverIO was not working well with Node 18, so I choose 16.20.2 since it was the latest stable version at the moment but feel free to check other versions by yourself.

You can easily install and maintain different node versions with `nvm` (Node Version Manager). You might already have it but if not, please install it first by following these instructions:

https://github.com/nvm-sh/nvm?tab=readme-ov-file#installing-and-updating

Once you have `nvm` ready, then we simple need to run

```bash
nvm install v16.20.2
```

If by any change, that version does not exist, then run 

```bash
nvm ls-remote
```

and choose another v16 available at that moment.

Just be sure that you are using the right node version by running this command on the root folder of this project.

```bash
nvm current v16.20.2
```

If you only have one version installed on your computer, then you don't need to do anything. Alternativly you can set that node version as defaut:

```bash
nvm alias default v16.20.2
```

Now that we have Node, then we need to install Appium. This will install it globally because their documentation shows it like that, but if you are against global installations, then remove the `-g` param.

```bash
npm i -g appium
```

if everthing is ok, then you can type appium and the demon wil start. 

```bash
appium
```

Finally we need to install one extra important module: UiAutomator2, which it is the Appium driver to use against native Android apps. You can install the driver easily with the following command:

```bash
appium driver install uiautomator2
```

## Doctor

There is a tool provided by appium to check all the requirements in your system. You need to install it with:

```bash
npm install @appium/doctor -g
```

and then just run 

```bash
appium-doctor --android
```

if you are thinking about using Appium against a native iOS app, then you can use `--ios` or just nothing and test both platforms. Obviously you will need to install a different driver and XCode.

One more thing, `appium-doctor` has been deprecated since Appium 2.4.0, so if it does not work for you, try just running the internal doctor feature that the driver already provides

```bash
appium plugin doctor uiautomator2
```

In my case `doctor` was complaining about not having `JAVA_HOME` environment variable but it is not needed, maybe becuase I am using a Javascript client instead of the Java one. Just take that into consideration if it doesn't work for you.


## Inspector

The Inspector app is a fundamental tool to help you debug and create tests. There is a online web app you can use or a native app. I found the native app faster and more stable than the web application but feel free to test both if you want. 

https://github.com/appium/appium-inspector

If you want to use the web app, you need to start `appium` with an extra parameter.

```bash
appium --allow-cors
```

The basic setup for initializing a simple Android session could be using the following capabilities:

```json
{
  "platformName": "Android",
  "appium:automationName": "UiAutomator2"
}
```

There are several different options, for more info you can check here:
https://appium.readthedocs.io/en/stable/en/writing-running-appium/caps/

Not every option will work because it depends how the app is actually coded. For example, using `appActivity` needs that we expose those screens and we are not doing it because of a security decision.  


## Configuring

Appium setup can be modified on `wdio.conf.js`, the project has a very simple setup but different parameters may work better for you. For example you may want that the app auto grant all the permissions or use a differnt device (check `adb devices`)

```json
{
    "capabilities": [{
        "platformName": "Android",
        "appium:automationName": "UiAutomator2",
        "appium:udid": "emulator-5554",
        "appium:autoGrantPermissions": true   
    }]
}
```

There are multimple different things you can configure, for example there are hooks that might be usefull when preparing your test just as "run X logic at the beginning of the suite" or "run X logic after running a test". Other parameters may include parameters to `Mocha`, `Spec` or `Allure`.


## Tests

We have only one 2e2 file with just two blocks, one for login and the other for getting to Receiving and scan something. We are using [PageObject](https://martinfowler.com/bliki/PageObject.html) pattern because it is looks like standard way to do it but you can use whatever architecture and design pattern you want.

Probably the most important thing about these tests is how we are "emulating" a scan action. The scanner is just a keyboard, so in theory typing on the screen (and then hitting ENTER) should be enought. However, I was not able to make it works and probably I should need now the backend actually is handling those key-strokes. So the solution was to just manually send a `Datawedge` event just like the real phone should have done. It looks complex and ugly but it will also test how we are internally handling the scanner in a much more realistic way. If you are planning to use a different client, then check the method `scan` on `pageobjects/page.js` just to see how you can emulate it yourself.

Just a frindly advice, when coding new test, just remember to wait until the elements are present on the screen. It is incredible important best practice when doing UI tests. For example, on Receiving, you should not just click and then scan cause getting to that page take some time so instead of that, just add a check in the middle that it will keep checking for a few seconds until it works and then you are completely sure you are ready to do the next operation:

```js
await MainPage.receiving()
await expect(ReceivingPage.title).toBeExisting() // it will wait until title is available or fail because of timeout
await MainPage.scan("something")
```

## Running the tests

If you have the `appium` deamon running, then close it otherwise the tests won't run cause they use the same port (by default is `4723`, but can be modified on `wdio.conf.js` or when running the `appium` command)

```bash
npx wdio wdio.conf.js
```


## Conclusion

This was just a very small showcase about this technology, it is by far not suttable to been use a final template. You maight even find Javascript not the right client for you. You can check Ruby, Python or Java for example.

The small tests that we run here only work if you are not logged in, and then you need to manually logout to run them again. Needless to say, this is not correct. But again, it is just to show how to do a simple workflow and answer a few question about Appium and see if actually works for us in the future.